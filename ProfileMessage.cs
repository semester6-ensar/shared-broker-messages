﻿using System;

namespace Shared.BrokerMessages
{
    public class ProfileMessage
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string Tag { get; set; }
        public string Name { get; set; }
        public string Avatar { get; set; }
    }
}
